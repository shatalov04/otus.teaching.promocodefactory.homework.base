﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected static ConcurrentDictionary<Guid, T> Data { get; private set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            var baseEntities = data as T[] ?? data.ToArray();

            lock (baseEntities)
            {
                if (!(Data is null))
                    return;

                Data = new ConcurrentDictionary<Guid, T>();

                foreach (var entity in baseEntities)
                    Data.TryAdd(entity.Id, entity);
            }
        }

        public Task AddAsync(T entity)
        {
            var task = new Task(() =>
            {
                var hasEntity = Data.TryGetValue(entity.Id, out _);

                if (hasEntity)
                    throw new ArgumentException($"Элемент с ID = {entity.Id} уже существует!");

                Data.TryAdd(entity.Id, entity);
            });

            task.Start();

            return task;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>) Data.Values);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            if (Data.TryGetValue(id, out var retrievedEntity))
                return Task.FromResult(retrievedEntity);

            throw new KeyNotFoundException($"Элемент с ID = {id} не найден!");
        }

        public Task UpdateAsync(T entity)
        {
            var task = new Task(() =>
            {
                var hasEntity = Data.TryGetValue(entity.Id, out var retrievedEntity);

                if (!hasEntity)
                    throw new ArgumentException(
                        $"Невозможно обновить элемент. Элемент с ID = {entity.Id} отсутствует!");

                Data.TryUpdate(entity.Id, entity, retrievedEntity);
            });

            task.Start();

            return task;
        }

        public Task DeleteAsync(Guid id)
        {
            var task = new Task(() =>
            {
                var hasEntity = Data.TryGetValue(id, out _);

                if (!hasEntity)
                    throw new ArgumentException($"Невозможно удалить элемент. Элемент с ID = {id} отсутствует!");

                Data.TryRemove(id, out _);
            });

            task.Start();

            return task;
        }
    }
}